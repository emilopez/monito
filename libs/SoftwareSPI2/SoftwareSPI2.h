#ifndef SoftwareSPI2_h
#define SoftwareSPI2_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

class SoftwareSPI2 {
public:
    SoftwareSPI2(byte, byte, byte);
    void begin();
    byte transfer(byte);
    void select();
    void deselect();

private:
    byte _pin_sck;
    //byte _pin_mosi;
    byte _pin_miso;
    byte _pin_cs;
};

#endif
