/* =================================================
   EQUIPO 2:  HUMEDAD DE SUELO
    - BASE: datalogger, DHT22, display OLED, boton MONITOR (puede ser analogico o digital segun puerto libre)
          Registra: ts; voltaje; HR; temp
   =================================================*/

#include <SD.h>
#include "Sodaq_DS3231.h"
#include <LowPower.h>
#include <DHT.h>
#include "SSD1306Ascii.h"
#include "SSD1306AsciiAvrI2c.h"
#include <SDI12.h>

#define I2C_ADDRESS 0x3C  // 0X3C+SA0 - 0x3C or 0x3D
#define RST_PIN -1        // Define proper RST_PIN if required.
#define DHTTYPE DHT22     // DHT 22  (AM2302)
#define DL_CS 10
#define PowDL 9
#define MONITORbtn A3
#define DHTPIN 4
#define DHT2PIN 7
#define BATPIN A6
#define HYDRAPIN 8         // Hydraprobe DATA PIN


DHT dht(DHTPIN, DHTTYPE);
DHT dht2(DHT2PIN, DHTTYPE);
SDI12 hydra(HYDRAPIN);
SSD1306AsciiAvrI2c oled;
File myFile;

uint16_t      MINUTOS[4] = {5, 15, 30, 60};
volatile byte IDX_MIN = 2;
bool          wakeUp = true;
uint32_t      count_sleep = 0;
int           sleep;
bool          boot;


void setup(){
    //Serial.begin(9600);
    pinMode(DL_CS, OUTPUT);
    pinMode(PowDL, OUTPUT);
    pinMode(DHTPIN, INPUT);
    pinMode(DHT2PIN, INPUT);
    digitalWrite(PowDL,HIGH);
    delay(100);
    rtc.begin();
    oled.begin(&Adafruit128x64, I2C_ADDRESS, RST_PIN);
    oled.setFont(System5x7);
    oled.setContrast(255);
    oled.set2X();
    delay(100);
    //rtc.setDateTime(DateTime(__DATE__, __TIME__));

    if (!SD.begin(DL_CS)) {
      oled.println(F("IOboot"));
    }else
      oled.println(F("OK"));
    oled.print(F("T="));
    oled.print(MINUTOS[IDX_MIN]);
    boot = true;
    delay(3000);
}

void loop(){
    unsigned long tiempo_inicio = millis();
    bool MONITOR = analogRead(MONITORbtn)>1000;
    if (wakeUp || MONITOR){
        // DESPERTAR E INICIALIZAR
        // =======================
        pinMode(DL_CS, OUTPUT);
        pinMode(PowDL, OUTPUT);
        digitalWrite(PowDL,HIGH);
        delay(100);
        rtc.begin();
        SD.begin(DL_CS);
        delay(900);
        dht.begin(); // el DHT necesita al menos 1 segundo para medir bien
        dht2.begin();
        hydra.begin();

        // MEDICION
        // ========
        char sep=';';

        hydra.sendCommand(F("1M!"));
        delay(300);                     // wait a while for a response
        while(hydra.available()){
            hydra.read();
            delay(5);
        }
        delay(1000);
        char cmd[3][5];
        strcpy(cmd[0], "1D0!");
        strcpy(cmd[1], "1D1!");
        strcpy(cmd[2], "1D2!");
        String hydraResponse="";

        for (byte s=0; s<3; s++){
            hydra.sendCommand(cmd[s]);
            delay(200);                     // wait a while for a response
            while(hydra.available()){       // write the response to the screen
                char c = hydra.read();
                if ((c != '\n') && (c != '\r')){
                    if (c == '+')
                        hydraResponse += sep;
                    else if (c == '-'){
                        hydraResponse += sep;
                        hydraResponse += '-';
                    }else
                        hydraResponse += c;
                    delay(5);
                }
            }
            delay(500);
        }
        //Serial.println(hydraResponse);
        delay(2100);
        byte hum_amb = dht.readHumidity();
        float tem_amb = dht.readTemperature();
        byte hum_amb2 = dht2.readHumidity();
        float tem_amb2 = dht2.readTemperature();

        // ALMACENA
        // =========
        if (wakeUp){
            char fname[13], yyyy[5], mm[3], dd[3];
            itoa(rtc.now().year(), yyyy, 10);
            itoa(rtc.now().month(), mm, 10);
            itoa(rtc.now().date(), dd, 10);
            if (strlen(mm)<2){
                mm[1] = mm[0];
                mm[0] = '0';
            }
            if (strlen(dd)<2){
                dd[1] = dd[0];
                dd[0] = '0';
            }
            mm[2]   = '\0';
            dd[2]   = '\0';
            yyyy[4] = '\0';

            strcpy(fname,yyyy);
            strcat(fname,mm);
            strcat(fname,dd);
            strcat(fname,".CSV");
            fname[12] = '\0';

            myFile = SD.open(fname, FILE_WRITE);
            delay(1000);
            if (myFile) {
                if (boot){
                    boot = false;
                    myFile.print(F("#HS;"));
                }
                myFile.print(rtc.now().year());
                myFile.print("-");
                myFile.print(rtc.now().month());
                myFile.print("-");
                myFile.print(rtc.now().date());
                myFile.print(" ");
                myFile.print(rtc.now().hour());
                myFile.print(":");
                myFile.print(rtc.now().minute());
                myFile.print(":");
                myFile.print(rtc.now().second());
                myFile.print(sep);
                myFile.print(1.5 * analogRead(BATPIN) * (5 / 1023.0));
                myFile.print(sep);
                myFile.print(hum_amb);
                myFile.print(sep);
                myFile.print(tem_amb);
                myFile.print(sep);
                myFile.print(hum_amb2);
                myFile.print(sep);
                myFile.print(tem_amb2);
                myFile.print(sep);
                myFile.print(hydraResponse);
                myFile.print(sep);
                byte j;
                for (j=0;j<10;j++){
                    myFile.print(analogRead(A0));
                    myFile.print(sep);
                    delay(5);
                }
                for (j=0;j<10;j++){
                    myFile.print(analogRead(A1));
                    myFile.print(sep);
                    delay(5);
                }
                for (j=0;j<9;j++){
                    myFile.print(analogRead(A2));
                    myFile.print(sep);
                    delay(5);
                }
                myFile.println(analogRead(A2));
                //Serial.println("saveOK");
            } else {
                //Serial.println(F("IO"));
                oled.println(F("IO"));
            }
            myFile.close();
                        // calcula tiempos que debe dormir
            // -------------------------------
            sleep = rtc.now().minute() % MINUTOS[IDX_MIN];
            if (sleep != 0){
                sleep = MINUTOS[IDX_MIN] - sleep;
            } else {
                sleep = MINUTOS[IDX_MIN];
            }
            sleep = sleep*60 - ((int)rtc.now().second());  //convierto a SEGUNDOS y resto segundos transcurridos
            sleep = sleep - (millis()-tiempo_inicio)/1000; // resto tiempo transcurrido hasta aca en segundos
            wakeUp = false;                                // a MiMiR
        }
        if (MONITOR){
            oled.begin(&Adafruit128x64, I2C_ADDRESS, RST_PIN);
            oled.setFont(System5x7);
            oled.setContrast(255);
            oled.clear();
            oled.set1X();
            //oled.println(ts);
            oled.print(rtc.now().year());
            oled.print("-");
            oled.print(rtc.now().month());
            oled.print("-");
            oled.print(rtc.now().date());
            oled.print(" ");
            oled.print(rtc.now().hour());
            oled.print(":");
            oled.print(rtc.now().minute());
            oled.print(":");
            oled.print(rtc.now().second());
            oled.println();
            oled.print(hum_amb2);  oled.print("%  "); oled.print(tem_amb2);oled.println("C  ");
            //oled.println();
            oled.set2X();

            oled.print("Hs:");
            byte p=3;
            while (hydraResponse[p] != ';'){
                oled.print(hydraResponse[p]);
                p++;
            }
            oled.println();
            p++;
            while (hydraResponse[p] != ';') p++;
            p++;
            oled.print("Ts:");
            while (hydraResponse[p] != ';'){
                oled.print(hydraResponse[p]);
                p++;
            }

            oled.set1X();
            oled.println();
            oled.println();
            oled.print("Hsc:");
            oled.print(analogRead(A0)); oled.print("|"); 
            oled.print(analogRead(A1)); oled.print("|"); 
            oled.println(analogRead(A2));
            oled.print(hum_amb);  oled.print("%  "); oled.print(tem_amb);
            oled.print("C  "); oled.print(1.5 * analogRead(BATPIN) * (5 / 1023.0)); oled.print("v");
            delay(3000);
        }

    }else{
        // DORMIR
        // ======
        digitalWrite(PowDL,LOW);                       // APAGA
        delay(100);
        count_sleep += 1;
        if (count_sleep >= sleep/4){
            wakeUp = true;
            count_sleep = 0;
        }
        LowPower.powerDown(SLEEP_4S, ADC_OFF, BOD_OFF);
        delay(100);
    }

}

/*
String get_timestamp(){
    DateTime now = rtc.now();
    String t = String(now.year()) + "-";
    t += (now.month()  < 10)? "0" + String(now.month() )   + "-": String(now.month())  + "-";
    t += (now.date()   < 10)? "0" + String(now.date()  )   + " ": String(now.date())   + " ";
    t += (now.hour()   < 10)? "0" + String(now.hour()  )   + ":": String(now.hour())   + ":";
    t += (now.minute() < 10)? "0" + String(now.minute())   + ":": String(now.minute()) + ":";
    t += (now.second() < 10)? "0" + String(now.second())        : String(now.second());

    return t;
}
*/
